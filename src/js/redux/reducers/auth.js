import { USER_LOADING, USER, USER_FAIL } from "./../types";

const INITIAL_STATE = {
  userLoading: false,
  user: {},
  userFail: ""
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case USER_LOADING:
      return {
        ...state,
        userLoading: action.payload
      };
    case USER:
      //we set flag isAuthentucated to true and check if it is not undefined then used the one which was dispatched
      //this is done because the same Type is being used for LogIn, LogOut and SignUp

      return {
        ...state,
        user: action.payload
      };
    case USER_FAIL:
      return {
        ...state,
        userFail: action.payload
      };
    default:
      return state;
  }
}
