import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import auth from "./auth";
import music from "./music";
import favorites from "./favorites";

export default combineReducers({
  auth,
  music,
  favorites,
  form: formReducer
});
