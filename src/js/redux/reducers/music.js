import { MUSIC_LOADING, MUSIC, MUSIC_FAIL } from "./../types";

const INITIAL_STATE = {
  musicLoading: false,
  music: [],
  musicFail: ""
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case MUSIC_LOADING:
      return {
        ...state,
        musicLoading: action.payload
      };
    case MUSIC:
      return {
        ...state,
        music: action.payload
      };
    case MUSIC_FAIL:
      return {
        ...state,
        musicFail: action.payload
      };
    default:
      return state;
  }
}
