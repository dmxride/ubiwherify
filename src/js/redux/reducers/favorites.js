import { FAVORITES_LOADING, FAVORITES, FAVORITES_FAIL } from "./../types";

const INITIAL_STATE = {
  favoritesLoading: false,
  favorites: [],
  favoritesFail: ""
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case FAVORITES_LOADING:
      return {
        ...state,
        favoritesLoading: action.payload
      };
    case FAVORITES:
      return {
        ...state,
        favorites: action.payload
      };
    case FAVORITES_FAIL:
      return {
        ...state,
        favoritesFail: action.payload
      };
    default:
      return state;
  }
}
