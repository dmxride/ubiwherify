export const USER_LOADING = "USER_LOADING";
export const USER = "USER";
export const USER_FAIL = "USER_FAIL";

export const MUSIC_LOADING = "MUSIC_LOADING";
export const MUSIC = "MUSIC";
export const MUSIC_FAIL = "MUSIC_FAIL";

export const LOGOUT = "LOGOUT";

export const FAVORITES_LOADING = "FAVORITES_LOADING";
export const FAVORITES = "FAVORITES";
export const FAVORITES_FAIL = "FAVORITES_FAIL";

export const FAVORITE_LOADING = "FAVORITE_LOADING";
export const FAVORITE = "FAVORITE";
export const FAVORITE_FAIL = "FAVORITE_FAIL";
