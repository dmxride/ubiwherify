import axios from "axios";
import { MUSIC_LOADING, MUSIC, MUSIC_FAIL } from "./../types";

export const getMusic = query => async dispatch => {
  let url = "https://songs-api-ubiwhere.now.sh/api/songs";
  if (query) {
    url += `?q=${query}&_sort=year`;
  }

  dispatch({ type: MUSIC_LOADING, payload: true });

  axios
    .get(url)
    .then(function(response) {
      // handle success
      dispatch({ type: MUSIC, payload: response.data });
    })
    .catch(function(error) {
      // handle error
      dispatch({ type: MUSIC_FAIL, payload: error.response.data.message });
    })
    .then(function() {
      dispatch({ type: MUSIC_LOADING, payload: false });
    });
};
