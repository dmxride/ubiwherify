import axios from "axios";
import {
  FAVORITES_LOADING,
  FAVORITES,
  FAVORITES_FAIL,
  FAVORITE_LOADING,
  FAVORITE,
  FAVORITE_FAIL
} from "./../types";
import { getToken } from "../../../cookies";

export const getFavorites = () => async dispatch => {
  dispatch({ type: FAVORITES_LOADING, payload: true });

  axios
    .get("https://songs-api-ubiwhere.now.sh/api/user-favorites/", {
      headers: { Authorization: `Bearer ${getToken()}` }
    })
    .then(function(response) {
      // handle success
      dispatch({ type: FAVORITES, payload: response.data });
    })
    .catch(function(error) {
      // handle error
      dispatch({ type: FAVORITES_FAIL, payload: error.response.data.message });
    })
    .then(function() {
      dispatch({ type: FAVORITES_LOADING, payload: false });
    });
};

export const postFavorites = id => async dispatch => {
  dispatch({ type: FAVORITE_LOADING, payload: true });

  axios
    .post(
      "https://songs-api-ubiwhere.now.sh/api/user-favorites",
      { songId: id },
      {
        headers: { Authorization: `Bearer ${getToken()}` }
      }
    )
    .then(function(response) {
      // handle success
      dispatch({ type: FAVORITE, payload: response.data });
    })
    .catch(function(error) {
      // handle error
      dispatch({ type: FAVORITE_FAIL, payload: error.response.data.message });
    })
    .then(function() {
      dispatch({ type: FAVORITE_LOADING, payload: false });
    });
};
