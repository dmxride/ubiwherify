import axios from "axios";
import { USER_LOADING, USER, USER_FAIL } from "./../types";
import { setToken, removeToken } from "../../../cookies";

export const signUp = data => async dispatch => {
  dispatch({ type: USER_LOADING, payload: true });

  axios
    .post("https://songs-api-ubiwhere.now.sh/api/auth/register", data)
    .then(function(response) {
      // handle success
      dispatch({
        type: USER,
        payload: {
          ...response.data,
          isAuthenticated: response.data.token
        }
      });
      //15000*60 sets a lifespan of 15 minutes to this token
      //(short lived tokens are usually used because they are safer)
      //it is usually set in the response header
      setToken(response.data.token, 15000 * 60);
    })
    .catch(function(error) {
      // handle error
      dispatch({
        type: USER_FAIL,
        payload: error.response.data.message
      });
    })
    .then(function() {
      dispatch({ type: USER_LOADING, payload: false });
    });
};

export const logIn = data => async dispatch => {
  dispatch({ type: USER_LOADING, payload: true });

  axios
    .post("https://songs-api-ubiwhere.now.sh/api/auth/login", data)
    .then(function(response) {
      // handle success
      dispatch({
        type: USER,
        payload: {
          ...response.data,
          isAuthenticated: response.data.token
        }
      });
      //15000*60 sets a lifespan of 15 minutes to this token
      //(short lived tokens are usually used because they are safer)
      //it is usually set in the response header
      setToken(response.data.token, 15000 * 60);
    })
    .catch(function(error) {
      // handle error
      dispatch({ type: USER_FAIL, payload: error.response.data.message });
    })
    .then(function() {
      dispatch({ type: USER_LOADING, payload: false });
    });
};

export const logOut = () => async dispatch => {
  dispatch({ type: USER, payload: { isAuthenticated: false } });
  removeToken();
};
