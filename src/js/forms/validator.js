import validator from "validator";

export const required = value => (value ? undefined : "This field is required");

export const maxLength = max => value =>
	value && value.length > max
		? `Must be ${max} characters or less`
		: undefined;

export const maxLength15 = maxLength(15);

export const number = value =>
	value && isNaN(Number(value)) ? "Must be a number" : undefined;

export const minValue = min => value =>
	value && value < min ? `Must be at least ${min}` : undefined;
export const minValue18 = minValue(18);

export const email = value =>
	value && !validator.isEmail(value) && "Invalid email address";

export const confirmPassword = (value, allValues) =>
	value !== allValues.password ? "Passwords don't match" : undefined;

export const tooOld = value =>
	value && value > 65 && "You might be too old for this";
