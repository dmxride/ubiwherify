import React from "react";
import "./Input.scss";
import { Input as AntInput, Select, Icon } from "antd";

const Input = props => {
  return (
    <div className="formInput">
      {props.label && <label htmlFor={props.name}>{props.label}</label>}
      <AntInput
        name={props.name}
        type={props.type}
        addonBefore={props.addLeft}
        addonAfter={props.addRight}
        autoComplete={props.autoComplete}
        placeholder={props.placeholder}
        defaultValue={props.defaultValue}
        className={props.feedbackInvalid ? "invalid" : ""}
        {...props.input}
      />
      <div className="formInput__warning">
        {props.feedbackInvalid && props.feedbackInvalid}
      </div>
    </div>
  );
};

export default Input;
