import React from "react";

import Input from "./elements/Input";

export const input = ({
  defaultValue,
  label,
  name,
  type,
  placeholder,
  autoComplete,
  addLeft,
  addRight,
  input,
  meta: { touched, error }
}) => {
  let feedbackInvalid = touched && error && error;

  return (
    <Input
      defaultValue={defaultValue}
      label={label}
      name={name}
      type={type}
      placeholder={placeholder}
      autoComplete={autoComplete}
      addRight={addRight}
      addLeft={addLeft}
      feedbackInvalid={feedbackInvalid}
      input={input}
    />
  );
};
