import React, { Component } from "react";
import { connect } from "react-redux";

export default ChildComponent => {
  //This is higher order componenet will handle checking if a user is logged in or not
  class ComposedComponent extends Component {
    // Our component will render
    componentWillMount() {
      this._shouldNavigateAway();
    }

    // Our component just got rendered
    componentDidMount() {
      this._shouldNavigateAway();
    }

    render() {
      return <ChildComponent {...this.props} />;
    }

    // Our component just got updated
    componentDidUpdate() {
      this._shouldNavigateAway();
    }

    _shouldNavigateAway() {
      if (!this.props.auth) {
        this.props.history.push("/");
      }
    }
  }

  function mapStateToProps(state) {
    return { auth: state.auth.user.isAuthenticated };
  }

  return connect(mapStateToProps)(ComposedComponent);
};
