import React, { Component } from "react";

import logo from "./../../../assets/logo.png";
import "./NavBar.scss";

import { connect } from "react-redux";
import * as actions from "./../../redux/actions";

import { Link, NavLink } from "react-router-dom";
import { Layout, Menu, Breadcrumb, Icon, Button } from "antd";
const { Header, Content, Footer, Sider } = Layout;

import Modal from "./../Modal/Modal";
import SignUp from "./Forms/SignUp";
import SignIn from "./Forms/SignIn";

class NavBar extends Component {
  signUpModal = React.createRef();
  signInModal = React.createRef();

  render() {
    return (
      <Sider className="sideMenu">
        <div className="sideMenu__logo">
          <img src={logo} alt="Ubiwherify" />
        </div>
        <Menu
          className="sideMenu__items"
          defaultSelectedKeys={["1"]}
          mode="inline"
        >
          <Menu.Item key="1">
            <Link to="/search">
              <Icon type="search" />
              <span>Search</span>
            </Link>
          </Menu.Item>
          {this.props.isAuthenticated && (
            <Menu.Item className="sideMenu__item" key="2">
              <Link to="/favorites">
                <Icon type="heart" />
                <span>Favorites</span>
              </Link>
            </Menu.Item>
          )}
        </Menu>
        <div className="sideMenu__authentication">
          {this.props.isAuthenticated && (
            <div>
              <Button onClick={this._onSignOut} block>
                LOG OUT
              </Button>
              <div>{this.props.userName}</div>
            </div>
          )}
          {!this.props.isAuthenticated && (
            <div>
              <Button onClick={this._onSignUp} block type="primary">
                SIGN UP
              </Button>
            </div>
          )}
          {!this.props.isAuthenticated && (
            <div>
              <Button onClick={this._onSignIn} ghost block>
                LOG IN
              </Button>
            </div>
          )}
        </div>
        <Modal title="Sign up with your email address" ref={this.signUpModal}>
          <SignUp
            onSubmit={this._onSignUpSubmit}
            onCancel={this._closeSignUpModal}
          />
        </Modal>
        <Modal title="Log In to your account" ref={this.signInModal}>
          <SignIn
            onSubmit={this._onSignInSubmit}
            onCancel={this._closeSignInModal}
          />
        </Modal>
      </Sider>
    );
  }

  _onSignUp = () => {
    this.signUpModal.current._showModal();
  };

  _onSignIn = () => {
    this.signInModal.current._showModal();
  };

  _onSignOut = () => {
    this.props.logOut();
  };

  _onSignUpSubmit = formValues => {
    this.props.signUp(formValues);
  };

  _onSignInSubmit = formValues => {
    this.props.logIn(formValues);
  };

  _closeSignUpModal = () => {
    this.signUpModal.current._onCancel();
  };
  _closeSignInModal = () => {
    this.signInModal.current._onCancel();
  };
}

function mapStateToProps(state) {
  return {
    userName: state.auth.user.name,
    isAuthenticated: state.auth.user.isAuthenticated
  };
}

export default connect(
  mapStateToProps,
  actions
)(NavBar);
