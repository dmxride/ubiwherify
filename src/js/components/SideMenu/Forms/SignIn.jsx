import React, { Component } from "react";
import "./_SignIn.scss";

import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "./../../../redux/actions";

import { reduxForm, Form, Field } from "redux-form";
import { input } from "./../../../forms/custom";
import { required, email } from "./../../../forms/validator";

import { Icon, Button, Alert } from "antd";

class SignUp extends Component {
  render() {
    let { handleSubmit, userFail } = this.props;

    return (
      <Form className="signInForm" onSubmit={handleSubmit(this._onSubmit)}>
        {userFail && <Alert message={userFail} type="error" closable />}
        <Field
          name="email"
          label="E-mail"
          type="text"
          placeholder="Email"
          addonBefore={<Icon type="mail" />}
          validate={[required, email]}
          component={input}
        />
        <Field
          name="password"
          label="Password"
          type="password"
          placeholder="Password"
          iconLeft="fas fa-lock"
          validate={[required]}
          component={input}
        />

        <Button htmlType="submit" block type="primary">
          Sign In
        </Button>
        <Button htmlType="button" onClick={this._onCancel} ghost block>
          Cancel
        </Button>
      </Form>
    );
  }

  componentDidUpdate(prevProps) {
    if (prevProps.user != this.props.user) {
      this._onCancel();
    }
  }

  _onSubmit = values => {
    console.log(values);
    this.props.onSubmit(values);
  };

  _onCancel = values => {
    this.props.reset();
    this.props.onCancel();
  };
}

function mapStateToProps(state) {
  return {
    userLoading: state.auth.userLoading,
    user: state.auth.user,
    userFail: state.auth.userFail
  };
}

export default compose(
  connect(
    mapStateToProps,
    actions
  ),
  reduxForm({
    form: "signup"
  })
)(SignUp);
