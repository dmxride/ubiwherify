import React, { Component } from "react";
import "./_SignUp.scss";

import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "./../../../redux/actions";

import { reduxForm, Form, Field } from "redux-form";
import { input } from "./../../../forms/custom";
import { required, email, confirmPassword } from "./../../../forms/validator";

import { Icon, Button } from "antd";

class SignUp extends Component {
  render() {
    let { handleSubmit } = this.props;

    return (
      <Form className="signUpForm" onSubmit={handleSubmit(this._onSubmit)}>
        <Field
          name="name"
          label="Username"
          type="text"
          placeholder="Username"
          addonBefore={<Icon type="user" />}
          validate={[required]}
          component={input}
        />

        <Field
          name="email"
          label="E-mail"
          type="text"
          placeholder="Email"
          addonBefore={<Icon type="mail" />}
          validate={[required, email]}
          component={input}
        />
        <Field
          name="password"
          label="Password"
          type="password"
          placeholder="Password"
          iconLeft="fas fa-lock"
          validate={[required]}
          component={input}
        />

        <Field
          name="confirmPassword"
          label="Confirm password"
          type="password"
          placeholder="Password"
          iconLeft="fas fa-lock"
          validate={[required, confirmPassword]}
          component={input}
        />

        <Button htmlType="submit" block type="primary">
          Sign Up
        </Button>
        <Button htmlType="button" onClick={this._onCancel} ghost block>
          Cancel
        </Button>
      </Form>
    );
  }

  componentDidUpdate(prevProps) {
    if (prevProps.user != this.props.user) {
      this._onCancel();
    }
  }

  _onSubmit = values => {
    this.props.onSubmit(values);
  };

  _onCancel = values => {
    this.props.reset();
    this.props.onCancel();
  };
}

function mapStateToProps(state) {
  return {
    userLoading: state.auth.userLoading,
    user: state.auth.user,
    userFail: state.auth.userFail
  };
}

export default compose(
  connect(
    mapStateToProps,
    actions
  ),
  reduxForm({
    form: "signup"
  })
)(SignUp);
