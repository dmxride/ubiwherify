import React, { Component } from "react";
import "./SearchInput.scss";

import { connect } from "react-redux";
import * as actions from "./../../../redux/actions";

class SearchInput extends Component {
  render() {
    return (
      <div className="fullInput">
        <input onChange={this._onSearch} placeholder="Start typing..." />
      </div>
    );
  }

  _onSearch = event => {
    this.props.getMusic(event.target.value);
  };
}

export default connect(
  null,
  actions
)(SearchInput);
