import React, { Component } from "react";
import "./MusicList.scss";

import _ from "lodash";

import { Icon } from "antd";
import Columns from "react-columns";

import { connect } from "react-redux";
import * as actions from "./../../../redux/actions";

import Music from "./../../General/Music/Music";

class MusicList extends Component {
  render() {
    let { music, favorites } = this.props;
    let queries = [
      {
        columns: 1,
        query: "min-width: 500px"
      },
      {
        columns: 3,
        query: "min-width: 800px"
      },
      {
        columns: 4,
        query: "min-width: 1000px"
      },
      {
        columns: 6,
        query: "min-width: 1360px"
      },
      {
        columns: 7,
        query: "min-width: 1600px"
      }
    ];

    return (
      <div>
        <Columns queries={queries} gap="16px">
          {music.map((musicItem, index) => {
            let isFavorited = false;

            if (favorites) {
              let favorite = _.findIndex(favorites, function(_favorite) {
                return _favorite.songId == musicItem.id;
              });

              if (favorite != -1) {
                isFavorited = true;
              }
            }

            return (
              <Music
                key={`music_${index}`}
                onLink={() => this._openMusic(musicItem.webUrl)}
                onUnFavorite={() => this._unfavoriteMusic(musicItem.id)}
                onFavorite={() => this._favoriteMusic(musicItem.id)}
                thumbnail={musicItem.imgUrl}
                isFavorited={isFavorited}
                showFavorite={this.props.isAuthenticated ? true : false}
                link={musicItem.webUrl}
                artist={musicItem.artist}
                title={musicItem.title}
                year={musicItem.year}
              />
            );
          })}
        </Columns>
        {music.length == 0 && <div className="empty">No content found</div>}
      </div>
    );
  }

  componentWillUpdate(prevProps) {
    if (prevProps.isAuthenticated != this.props.isAuthenticated) {
      if (this.props.isAuthenticated) {
        this.props.getFavorites();
      }
    }
  }

  componentDidMount() {
    this.props.getMusic();
    if (this.props.isAuthenticated) {
      this.props.getFavorites();
    }
  }

  _openMusic = link => {
    window.open(link, "_blank");
  };

  _favoriteMusic = id => {
    this.props.postFavorites(id);
  };

  _unfavoriteMusic = id => {
    this.props.postUnfavorites(id);
  };
}

function mapStateToProps(state) {
  return {
    musicLoad: state.music.musicLoading,
    music: state.music.music,
    isAuthenticated: state.auth.user.isAuthenticated,
    favoritesLoad: state.favorites.favoritesLoading,
    favorites: state.favorites.favorites
  };
}

export default connect(
  mapStateToProps,
  actions
)(MusicList);
