import React, { Component } from "react";
import "./FavoritesList.scss";

import Columns from "react-columns";

import { connect } from "react-redux";
import { compose } from "redux";

import * as actions from "./../../../redux/actions";
import Music from "./../../General/Music/Music";

import _ from "lodash";
import requireAuth from "./../../HOC/requireAuth";

class FavoritesList extends Component {
  state = {
    myFavorites: []
  };

  render() {
    let { myFavorites } = this.state;
    let queries = [
      {
        columns: 1,
        query: "min-width: 500px"
      },
      {
        columns: 3,
        query: "min-width: 800px"
      },
      {
        columns: 4,
        query: "min-width: 1000px"
      },
      {
        columns: 6,
        query: "min-width: 1360px"
      },
      {
        columns: 7,
        query: "min-width: 1600px"
      }
    ];
    return (
      <div>
        <Columns queries={queries} gap="16px">
          {myFavorites.map((musicItem, index) => {
            return (
              <Music
                key={`music_${index}`}
                onLink={() => this._openMusic(musicItem.webUrl)}
                onUnFavorite={() => this._unfavoriteMusic(musicItem.id)}
                thumbnail={musicItem.imgUrl}
                isFavorited={true}
                showFavorite={true}
                link={musicItem.webUrl}
                artist={musicItem.artist}
                title={musicItem.title}
                year={musicItem.year}
              />
            );
          })}
        </Columns>
        {myFavorites.length == 0 && (
          <div className="empty">No Favorited content</div>
        )}
      </div>
    );
  }

  componentDidMount() {
    this.props.getFavorites();
    this.props.getMusic();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.music != this.props.music) {
      if (!_.isEmpty(this.props.favorites)) {
        this._setFavorites();
      }
    }

    if (prevProps.favorites != this.props.favorites) {
      if (!_.isEmpty(this.props.music)) {
        this._setFavorites();
      }
    }
  }

  _setFavorites = () => {
    let myFavorites = [];

    _.each(this.props.favorites, (favorite, key) => {
      myFavorites.push(
        _.find(this.props.music, function(music) {
          return music.id == favorite.songId;
        })
      );
    });

    this.setState({ myFavorites });
  };

  _openMusic = link => {
    window.open(link, "_blank");
  };

  _unfavoriteMusic = id => {
    this.props.postUnfavorites(id);
  };
}

function mapStateToProps(state) {
  return {
    favoritesLoad: state.favorites.favoritesLoading,
    favorites: state.favorites.favorites,
    musicLoad: state.music.musicLoading,
    music: state.music.music
  };
}

export default compose(
  connect(
    mapStateToProps,
    actions
  ),
  requireAuth
)(FavoritesList);
