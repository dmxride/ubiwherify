import React from "react";
import { Layout } from "antd";
const { Header, Footer, Sider, Content } = Layout;

import NavBar from "./SideMenu/NavBar";
import SearchInput from "./Search/SearchInput/SearchInput";
import MusicList from "./Search/MusicList/MusicList";
import FavoritesList from "./Favorites/FavoritesList/FavoritesList";
import FavoritesSearch from "./Favorites/FavoritesSearch/FavoritesSearch";

import { BrowserRouter, Route, Redirect, Switch } from "react-router-dom";

const App = () => {
  return (
    <BrowserRouter>
      <Layout>
        <Sider>
          <Route exact path="/" render={() => <Redirect to="/search" />} />
          <NavBar />
          />
        </Sider>
        <Layout>
          <Header>
            <Switch>
              <Route path="/search" component={SearchInput} />
              <Route path="/favorites" component={FavoritesSearch} />
            </Switch>
          </Header>
          <Content>
            <Switch>
              <Route path="/search" component={MusicList} />
              <Route path="/favorites" component={FavoritesList} />
            </Switch>
          </Content>
        </Layout>
      </Layout>
    </BrowserRouter>
  );
};

export default App;
