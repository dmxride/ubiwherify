import React, { Component } from "react";
import "./Music.scss";

import { connect } from "react-redux";

import { Icon } from "antd";

class Music extends Component {
  state = {
    hover: false
  };
  render() {
    return (
      <div
        onMouseEnter={this._onHover}
        onMouseLeave={this._onBlur}
        className="musicItem"
      >
        <div className="musicItem__cover">
          <div className="musicItem__artist">{this.props.artist}</div>
          <img alt={this.props.artist} src={this.props.thumbnail} />

          {this.state.hover && (
            <div className="musicItem__icons-link">
              <Icon
                onClick={() => this.props.onLink(this.props.link)}
                type="link"
              />
            </div>
          )}

          {this.props.showFavorite && (
            <div className="musicItem__icons">
              {this.props.isFavorited ? (
                <Icon
                  onClick={() => this.props.onUnFavorite(this.props.id)}
                  type="heart"
                  theme="filled"
                />
              ) : (
                <Icon
                  onClick={() => this.props.onFavorite(this.props.id)}
                  type="heart"
                />
              )}
            </div>
          )}
        </div>
        <div className="musicItem__title">
          {this.props.title} | {this.props.year}
        </div>
      </div>
    );
  }

  _onHover = () => {
    this.setState({ hover: true });
  };

  _onBlur = () => {
    this.setState({ hover: false });
  };
}

export default Music;
