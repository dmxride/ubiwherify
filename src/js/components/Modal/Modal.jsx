import React, { Component } from "react";
import { Modal as AntModal } from "antd";

class Modal extends Component {
  state = { visible: false };

  render() {
    return (
      <AntModal
        title={this.props.title}
        visible={this.state.visible}
        footer={null}
        onCancel={this._onCancel}
      >
        {this.props.children}
      </AntModal>
    );
  }

  _showModal = () => {
    this.setState({
      visible: true
    });
  };

  _onSubmit = e => {
    this.setState({
      visible: false
    });
  };

  _onCancel = e => {
    this.setState({
      visible: false
    });
  };
}

export default Modal;
