import React from "react";
import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import reduxThunk from "redux-thunk";
import reducers from "./js/redux/reducers";

import { getToken } from "./cookies";

import App from "./js/components/App";

import "./styles/index.scss";

const store = createStore(
  reducers,
  {
    auth: {
      user: { isAuthenticated: getToken() }
    }
  },
  applyMiddleware(reduxThunk)
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
